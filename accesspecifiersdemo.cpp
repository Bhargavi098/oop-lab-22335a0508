#include<iostream>
using namespace std;
class AccessSpecifierDemo{
   private:
       int priVar;
   protected:
       int proVar;
   public:
       int pubVar;
   public:
        void setVar(int priValue,int proValue, int pubValue){
           priVar = priValue;
           proVar = proValue;
           pubVar = pubValue;
        }
   public:
       void getVar(){
           cout<<"private variable is : "<<priVar<<endl;
           cout<<"protected variable is : "<<proVar<<endl;
           cout<<"public variable is : "<<pubVar<<endl; 
       }
};
int main()
{
   AccessSpecifierDemo obj;
   obj.setVar(60,90,120);
   obj.getVar();
}
