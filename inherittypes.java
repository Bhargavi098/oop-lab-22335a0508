class Child1
{
   int a=10;
   void display1()
   {
       System.out.println("Content in Child 1 :"+a);
   }
}
//simple inheritance
class Child2 extends Child1
{
   int b=10;
   int c=a+b;
   void display2()
   {
       System.out.println("Content in Child 2:"+c);
   }
}
//Multilevel Inheritance
class Child3 extends Child2
{
   int d=30;
   int e=c+d;
   void display3()
   {
       System.out.println("Content in Child 3:"+e);
   }
}
//Hierarchical Inheritance
class Child4 extends Child1
{
   int f=40;
   int x=a+f;
   void display4()
   {
       System.out.println("Content in child 4:"+x);
   }
}
//Hybrid Inheritance
class Child5 extends Child2
{
   int g=50;
   int y=b+g;
   void display5()
   {
       System.out.println("Content in Child 5:"+y);
   }
}
class inherit{
   public static void main(String args[])
   {
       Child2 ob1=new Child2();
       ob1.display2();
       Child3 ob2=new Child3();
       ob2.display3();
       Child4 ob3=new Child4();
       ob3.display4();
       Child5 ob4=new Child5();
       ob4.display5();
   }


}
