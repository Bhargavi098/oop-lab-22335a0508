interface A{
   public default void display(){
       System.out.println("class A display() method called");


   } 
} 
interface B{
   public default void display(){
       System.out.println("class B display() method called");


   } 
} 
class soldiamondprob implements A, B{
   public void display(){
       A.super.display();
       B.super.display(); 
   }
   public static void main(String args[]){
       soldiamondprob obj = new soldiamondprob();
       obj.display(); 
   } 
}


