import java.io.*;
class Sample extends Thread {
	public void run()
	{
		System.out.print("Creation of Threads.");
	}
	public static void main(String[] args)
	{
		Sample s = new Sample(); // creating thread
		s.start(); // starting thread
	}
}
