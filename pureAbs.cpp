#include <iostream>
using namespace std;
class pureAbs
{
public:
    virtual void display() = 0;
};
class child1 : public pureAbs
{
public:
    void display()
    {
        cout << "Welcome to my program!" << endl;
    }
};
class child2: public pureAbs
{
    void display()
    {
        cout << "Pure Abstraction using Virtual Functions" << endl;
    }
};
int main()
{
    pureAbs* fun1 = new child1();
    pureAbs* fun2 = new child2();
    fun1->fun();
    fun2->fun();
    delete fun1;
    delete fun2;
    return 0;
}
