import java.util.HashMap;
import java.util.Map;
public class MapDemo {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        map.put("apple", 1);
        map.put("banana", 2);
        map.put("orange", 3);
        System.out.println("Map size: " + map.size());
        int value = map.get("banana");
        System.out.println("Value for key 'banana': " + value);
        map.remove("orange");
        boolean containsKey = map.containsKey("apple");
        System.out.println("Map contains key 'apple': " + containsKey);
        boolean containsValue = map.containsValue(2);
        System.out.println("Map contains value 2: " + containsValue);
        for (String key : map.keySet()) {
            System.out.println("Key: " + key + ", Value: " + map.get(key));
        }
    }
}
