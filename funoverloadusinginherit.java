class Shape {
    public void draw() {
        System.out.println("Drawing a shape");
    }
}
class Circle extends Shape {
    public void draw() {
        System.out.println("Drawing a circle");
    }
    public void draw(int radius) {
        System.out.println("Drawing a circle with radius " + radius);
    }
}
class Drawing{
    public static void main(String[]args){
        Shape shape = new Shape();
        shape.draw();
        Circle circle = new Circle();
        circle.draw();
        circle.draw(5);
    }
}
