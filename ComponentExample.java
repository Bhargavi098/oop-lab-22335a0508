import java.awt.*;
import java.awt.event.*;

public class ComponentExample extends Frame {
   public ComponentExample() {
      // Set the title of the window
      setTitle("Component Example");

      // Set the size of the window
      setSize(300, 200);

      // Create a button component
      Button button = new Button("Click me");

      // Add the button to the window
      add(button);

      // Create a listener for the button
      button.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            System.out.println("Button clicked!");
         }
      });

      // Show the window
      setVisible(true);
   }

   public static void main(String[] args) {
      new ComponentExample();
   }
}
