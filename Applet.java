import java.util.ArrayList;
import java.util.List;
public class CollectionsDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("apple");
        list.add("banana");
        list.add("orange");
        System.out.println("List size: " + list.size());
        for (String element : list) {
            System.out.println(element);
        }
        list.remove("banana");
        System.out.println("Updated list: " + list);
    }
}
