class Student2 {
   String fullName;
   double semPercentage;
   String collegeName;
   int collegeCode;
   // default constructor
   Student2(){
       collegeName = "MVGR";
       collegeCode = 33;
       System.out.println("Default Constructor Called");
       System.out.println("College Name : " + collegeName);
       System.out.println("College Code : " + collegeCode);
   }
 // Parameterized Constructor
 Student2(String fname, double percentage){
   fullName = fname;
   semPercentage = percentage;
   System.out.println("Parameterized Constructor Called");
   System.out.println("Name : " + fullName);
   System.out.println("Sem Percentage : " + semPercentage);
}
protected void finalize(){
   System.out.println("Deallocated");
}
//main function
public static void main(String[] args){
   Student2 obj1 = new Student2();
   Student2 obj2 = new Student2("Bhargavi", 8.5);
   obj1.finalize();
   obj2.finalize();
}
}
