class TestFinallyBlock {    
 public static void main(String args[]){    
 try{    
 int data=25/5;    
 System.out.println("The result of without final block:"+data);    
 }        
finally {  
System.out.println("finally block is always executed");  
}    
}    
} 
