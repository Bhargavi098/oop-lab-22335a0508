class Boxing {
    public void boxingUnBoxing() {
        int counter = 30;
        Integer boxedCounter = counter;
        System.out.println("Boxed Counter: " + boxedCounter);
        int unboxedCounter = boxedCounter;
        System.out.println("Unboxed Counter: " + unboxedCounter);
    }
    public static void main(String[] args) {
        Boxing obj = new Boxing();
        obj.boxingUnBoxing();
    }
}
