class MutableDemo {
    private String studentName;
    private int rollNumber;
    MutableDemo (String studentName, int rollNumber) {
    this.studentName = studentName;
    this.rollNumber = rollNumber;
    }
    public String getStudentName() {
    return studentName;
    }
    public void setStudentName(String studentName) {
    this.studentName = studentName;
    }
    public int getRollNumber() {
    return rollNumber;
    }
    public void setRollNumber(int rollNumber) {
    this.rollNumber = rollNumber;
    }
    public static void main(String[] args) {
    MutableDemo obj = new MutableDemo ("Bhargavi" , 508);
    System.out.println("Original Name is " + obj.getStudentName());
    System.out.println("\n Original Roll Number is " + obj.getRollNumber());
    obj.setStudentName("Sailu");
    obj.setRollNumber(516);
    System.out.println("Modified Name is " + obj.getStudentName());
    System.out.println("Modified Roll Number is " + obj.getRollNumber());
    }
    }
