#include<iostream>
using namespace std;
class Demo
{
   int real,img,item;
   public:
   Demo(int realnum,int imgnum)
   {
       real=realnum;
       img=imgnum;
   }
   void operator=(Demo &obj)
   {
       real=obj.real;
       img=obj.img;
   }
   void displayvalues()
   {
       cout<<"Real number:"<<real<<endl;
       cout<<"Imaginary number:"<<img<<endl;
   }
};
int main()
{
   Demo obj1(5,9);
   Demo obj2=obj1;
   obj1.displayvalues();
}
